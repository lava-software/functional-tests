#!/usr/bin/env python3

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


import os
import sys
import argparse
import tempfile
import subprocess

# pylint: disable=superfluous-parens,missing-docstring
# pylint: disable=too-many-instance-attributes

# How to use this script
#
# Create a Stretch LXC
#
# Write the script into the container
# Pass --lava if inside a LAVA test job.

# Still a work-in-progress.

# packages to install from stable
FIRST_INSTALL = [
    'postgresql', 'python-pkg-resources', 'wget',
    'lava-dispatcher', 'lava-server'
]

# choice of mirror for backports
BACKPORTS_MIRROR = 'http://mirror.bytemark.co.uk/debian'

# packages to install explicitly from backports.
BACKPORTS_LIST = [
    'python3-pexpect', 'python3-django', 'python3-django-tables2',
    'python3-voluptuous', 'python3-pyudev',
    'python3-django-restricted-resource', 'lxc', 'debootstrap']

LAVA_LIST = ['lava-dispatcher', 'lava-server']


def environment():
    os.environ['LANGUAGE'] = 'C.UTF-8'
    os.environ['LC_ALL'] = 'C.UTF-8'
    os.environ['LANG'] = 'C.UTF-8'
    os.environ['DEBIAN_FRONTEND'] = 'noninteractive'
    os.environ['DJANGO_COLORS'] = 'nocolor'


def execute(command):
    process = subprocess.Popen(
        command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == '' and process.poll() is not None:
            break
        sys.stdout.write(nextline)
        sys.stdout.flush()

    exit_code = process.returncode

    return 'pass' if (exit_code == 0) else 'fail'


class LtsTest(object):

    def __init__(self, lava=False, suite='jessie'):
        super(LtsTest, self).__init__()
        self.lava = lava
        self.results = []  # list of dictionaries
        self.ci_dir = None
        self.source_dir = None
        self.old_dir = None
        if suite == 'stretch':
            self.manage_helper = '--no-input'
            self.sloppy_suite = None
            self.backport_suite = 'stretch-backports'
        else:
            raise ValueError("Unrecognised suite: %s" % suite)

    def installation(self):
        with open('/etc/resolv.conf', 'a') as nameserver:
            nameserver.write("nameserver 8.8.8.8\n")
        execute('apt -q update')
        execute('apt -q -y upgrade')
        ret = execute('apt -y install %s' % " ".join(FIRST_INSTALL))
        self.report_case('postgres', result=ret)
        with open('/etc/apt/sources.list.d/lava-backports.list', 'w') as backports:
            backports.write(
                "deb %s %s main\n" % (BACKPORTS_MIRROR, self.backport_suite))
            backports.write(
                "deb-src %s %s main\n" % (BACKPORTS_MIRROR, self.backport_suite))
            if self.sloppy_suite:
                backports.write(
                    "deb %s %s main\n" % (BACKPORTS_MIRROR, self.sloppy_suite))
                backports.write("deb-src %s %s main\n" % (
                    BACKPORTS_MIRROR, self.sloppy_suite))
        execute('apt -q update')
        execute('apt -q -y -t %s install %s' % (
            self.backport_suite, " ".join(BACKPORTS_LIST)))
        if self.sloppy_suite:
            execute(
                'apt-get -q -y -t %s install %s' % (
                    self.sloppy_suite, " ".join(LAVA_LIST)))
        else:
            execute('apt -q -y -t %s install %s' % (
                self.backport_suite, " ".join(LAVA_LIST)))
        ret = execute('dpkg-query -W lava-server')
        self.report_case('dpkg', result=ret)

    def prepare_source(self):
        execute(
            'apt -q -y '
            '--no-install-recommends install devscripts')
        execute('apt -q -y -t %s install lava-dev' % self.backport_suite)
        self.ci_dir = tempfile.mkdtemp()
        self.old_dir = os.getcwd()
        os.chdir(self.ci_dir)
        if self.sloppy_suite:
            execute('apt-get source -t %s lava-server' % self.sloppy_suite)
        else:
            execute('apt-get source -t %s lava-server' % self.backport_suite)
        for item in os.listdir(self.ci_dir):
            if os.path.isdir(item):
                self.source_dir = os.path.join(self.ci_dir, item)
                os.chdir(self.source_dir)
                break

    def enable_apache(self):
        ret = execute('a2ensite lava-server')
        self.report_case('ensite', result=ret)
        execute('a2dissite 000-default')
        execute('a2enmod proxy')
        execute('a2enmod proxy_http')
        ret = execute('apache2ctl restart')
        self.report_case('apache2', result=ret)

    def sanity_check(self):
        ret = execute('service lava-server status')
        self.report_case('server', result=ret)
        ret = execute('service lava-master status')
        self.report_case('master', result=ret)
        ret = execute('service lava-slave status')
        self.report_case('slave', result=ret)
        execute('lava-server manage migrate %s' % self.manage_helper)
        ret = execute('lava-server manage devices list')
        self.report_case('devicedict', result=ret)

    def version_wrapper(self):
        os.chdir(self.source_dir)
        result = "pass"
        try:
            ver = subprocess.check_output(["./version.py"])
        except OSError:
            result = "fail"
            ver = ''
        except subprocess.CalledProcessError:
            result = "fail"
            ver = ''
        self.report_case('code-version-%s' % ver.strip(), result)
        os.chdir(self.old_dir)

    def report_case(self, name, result='pass'):
        self.results.append({name: result})
        if self.lava:
            os.system("lava-test-case %s --result %s" % (name, result))
        else:
            print("%s --result %s" % (name, result))

    def unittests(self):
        os.chdir(self.source_dir)
        ret = execute(
            './lava_server/manage.py test --noinput -v2 '
            'lava_scheduler_app lava_scheduler_daemon '
            'linaro_django_xmlrpc.tests lava_results_app')
        self.report_case('lava-server-manage-test', result=ret)
        os.chdir(self.old_dir)

    def summarise(self):
        for case in self.results:
            for name, result in case.items():
                print("%s: %s" % (name, result))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--lava', dest='lava', action='store_true', help='call lava-test-case')
    parser.add_argument(
        '--suite', dest='suite', default='stretch', help='Debian suite being tested')
    args = parser.parse_args()
    lava_arg = vars(args)['lava']
    suite_arg = vars(args)['suite']
    environment()
    lts = LtsTest(lava=lava_arg, suite=suite_arg)
    lts.installation()
    lts.enable_apache()
    lts.sanity_check()
    lts.prepare_source()
    lts.version_wrapper()
    lts.unittests()
    lts.summarise()


if __name__ == '__main__':
    main()
