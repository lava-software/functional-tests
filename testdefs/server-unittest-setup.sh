#!/bin/sh

# dispatcher unit test setup

. ./testdefs/lava-common

APTOPTS="-q -y install --no-install-recommends -o Dpkg::Options::=--force-confold"
LAVAOPTS="-q -y install --no-install-recommends"
TOOLS="git ca-certificates wget python3 python3-yaml gnupg"

command 'update-apt' "apt-get update -q"
export DEBIAN_FRONTEND=noninteractive
unset LANG
unset LANGUAGE
command 'install-tools' "apt-get ${APTOPTS} ${TOOLS}"
command 'clone-lava' "git clone -q https://git.lavasoftware.org/lava/lava.git"
DEPS=`./lava/share/requires.py -s stretch -p lava-server -d debian -n`
command 'install-base' "apt-get ${APTOPTS} ${DEPS}"
DEPS=`./lava/share/requires.py -s stretch -p lava-server -d debian -n -u`
command 'install-testdeps' "apt-get ${APTOPTS} ${DEPS}"

command 'apache' "apt-get ${APTOPTS} apache2"
command "get-staging-key" 'wget https://apt.lavasoftware.org/lavasoftware.key.asc'
apt-key add lavasoftware.key.asc
rm lavasoftware.key.asc
echo "deb http://mirror.bytemark.co.uk/debian stretch-backports main" > /etc/apt/sources.list.d/lava-backports.list
echo "deb http://apt.lavasoftware.org/debian stretch-backports main" > /etc/apt/sources.list.d/lava-staging.list
command 'update-apt-with-new-key' "apt-get update -q"
DEPS=`./lava/share/requires.py -s stretch-backports -p lava-server -d debian -n`
command 'install-backports-base' "apt-get -t stretch-backports ${APTOPTS} ${DEPS}"
DEPS=`./lava/share/requires.py -s stretch-backports -p lava-server -d debian -n -u`
command 'install-backports-testdefps' "apt-get -t stretch-backports ${APTOPTS} ${DEPS}"
command 'install-devscripts' "apt-get ${APTOPTS} devscripts"
command 'install-server' "apt-get ${LAVAOPTS} -t stretch-backports lava-server"
apt-get clean
# fake up NFS support
touch /usr/sbin/exportfs
