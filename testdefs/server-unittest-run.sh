#!/bin/sh

# dispatcher unit tests run

. ./testdefs/lava-common

sh -ex ./testdefs/server-unittest-setup.sh

command 'source_directory' "ls ./lava"
cd lava
testcase 'test_directory' "ls ../functional"
testcase "commitlog" 'git --no-pager log -n1'
../functional/version-wrapper.py
${LAVA_SET} start unittests
testcase 'unittests' "../functional/server-unittests.sh"
${LAVA_SET} stop unittests
