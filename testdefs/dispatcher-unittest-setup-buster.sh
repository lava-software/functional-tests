#!/bin/sh

# dispatcher unit test setup

. ./testdefs/lava-common

APTOPTS="-q -y install --no-install-recommends -o Dpkg::Options::=--force-confold"
LAVAOPTS="-q -y install --no-install-recommends -o Dpkg::Options::=--force-confnew"
TOOLS="git ca-certificates wget python3 python3-yaml gnupg"

command 'update-apt' "apt-get update -q"
export DEBIAN_FRONTEND=noninteractive
unset LANG
unset LANGUAGE
command 'install-tools' "apt-get ${APTOPTS} ${TOOLS}"
command 'clone-lava' "git clone -q https://git.lavasoftware.org/lava/lava.git"
DEPS=`./lava/share/requires.py -s buster -p lava-dispatcher -d debian -n`
command 'install-base' "apt-get ${APTOPTS} ${DEPS}"
DEPS=`./lava/share/requires.py -s buster -p lava-dispatcher -d debian -n -u`
command 'install-testdeps' "apt-get ${APTOPTS} ${DEPS}"
# fix up the hosts file before installing apache
echo 127.0.0.1   localhost `hostname` >> /etc/hosts
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
command 'apache' "apt-get ${APTOPTS} apache2"
command "get-staging-key" 'wget https://apt.lavasoftware.org/lavasoftware.key.asc'
apt-key add lavasoftware.key.asc
rm lavasoftware.key.asc
echo "deb http://apt.lavasoftware.org/debian buster main" > /etc/apt/sources.list.d/lava-staging.list
command 'update-apt-with-new-key' "apt-get update -q"
command 'install-dispatcher' "apt-get ${LAVAOPTS} lava-dispatcher"
apt-get clean
# fake up NFS support
touch /usr/sbin/exportfs
