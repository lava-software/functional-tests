#!/bin/sh

# dispatcher unit test setup

. ./testdefs/lava-common

APTOPTS="-q -y install --no-install-recommends -o Dpkg::Options::=--force-confold"
LAVAOPTS="-q -y install --no-install-recommends"
TOOLS="git ca-certificates wget python3 python3-yaml gnupg"

command 'update-apt' "apt-get update -q"
export DEBIAN_FRONTEND=noninteractive
unset LANG
unset LANGUAGE
command 'install-tools' "apt-get ${APTOPTS} ${TOOLS}"
command 'clone-lava' "git clone -q https://git.lavasoftware.org/lava/lava.git"
DEPS=`./lava/share/requires.py -s stretch -p lava-dispatcher -d debian -n`
command 'install-base' "apt-get ${APTOPTS} ${DEPS}"
DEPS=`./lava/share/requires.py -s stretch -p lava-dispatcher -d debian -n -u`
command 'install-testdeps' "apt-get ${APTOPTS} ${DEPS}"
command 'apache' "apt-get ${APTOPTS} apache2"
# unit tests need to check existence of files on files.lavasoftware.org
command "check-community-connection" "wget https://files.lavasoftware.org/components/lava/standard/debian/"
rm index.html
command "get-staging-key" 'wget https://apt.lavasoftware.org/lavasoftware.key.asc'
apt-key add lavasoftware.key.asc
rm lavasoftware.key.asc
# unit tests need to check existence of files on images.validation.linaro.org
command "check-linaro-connection" "wget http://images.validation.linaro.org/snapshots.linaro.org/components/lava/standard/debian/"
rm index.html
echo "deb http://mirror.bytemark.co.uk/debian stretch-backports main" > /etc/apt/sources.list.d/lava-backports.list
echo "deb http://apt.lavasoftware.org/debian stretch-backports main" > /etc/apt/sources.list.d/lava-staging.list
command 'update-apt-with-new-key' "apt-get update -q"
DEPS=`./lava/share/requires.py -s stretch-backports -p lava-dispatcher -d debian -n`
if [ -n "${DEPS}" ]; then
    command 'install-backports-base' "apt-get -t stretch-backports ${APTOPTS} ${DEPS}"
fi
DEPS=`./lava/share/requires.py -s stretch-backports -p lava-dispatcher -d debian -n -u`
if [ -n "${DEPS}" ]; then
    command 'install-backports-testdefps' "apt-get -t stretch-backports ${APTOPTS} ${DEPS}"
fi
command 'install-dispatcher' "apt-get ${LAVAOPTS} -t stretch-backports lava-dispatcher"
apt-get clean
# fake up NFS support
touch /usr/sbin/exportfs
