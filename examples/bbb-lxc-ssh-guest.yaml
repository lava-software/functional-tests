# submission YAML prototype for connecting to a BBB over ssh
# as secondary connection.
# whichever role is operating as the "host" must specify how to
# authorize connections from other roles using the authorize: key
# in the deployment. This allows the relevant Action to deploy the
# necessary support. e.g. /root/.ssh/authorized_keys

job_name: LXC and secondary connection with a device
timeouts:
  job:
    minutes: 30
  action:
    minutes: 3
  connection:
    minutes: 5
priority: medium
visibility: public

metadata:
  source: https://git.lavasoftware.org/lava/functional-tests.git
  path: bbb-lxc-ssh-guest.yaml
  build-readme: http://files.lavasoftware.org/components/lava/standard/debian/stretch/armhf/2/debian-stretch-armmp-armhf-readme.html
  build-console: https://ci.linaro.org/view/lava-ci/job/lava-debian-stretch-armmp-armhf/2/console
  build-script: http://files.lavasoftware.org/components/lava/standard/debian/stretch/armhf/2/armmp-nfs.sh

protocols:
  lava-lxc:
    host:
      name: lxc-ssh-test
      template: debian
      distribution: debian
      release: jessie
  lava-multinode:
    # expect_role is used by the dispatcher and is part of delay_start
    # host_role is used by the scheduler, unrelated to delay_start.
    roles:
      host:
        device_type: beaglebone-black
        count: 1
        timeout:
          minutes: 10
      guest:
        # protocol API call to make during protocol setup
        request: lava-start
        # set the role for which this role will wait
        expect_role: host
        timeout:
          minutes: 15
        # no device_type, just a connection
        connection: ssh
        count: 3
        # each ssh connection will attempt to connect to the device of role 'host'
        host_role: host

actions:
- deploy:
    role:
    - host
    namespace: probe
    timeout:
      minutes: 5
    to: lxc
    packages:
    - usbutils
    - procps
    - lsb-release
    - util-linux

- boot:
    role:
    - host
    namespace: probe
    prompts:
    - 'root@(.*):/#'
    timeout:
      minutes: 5
    method: lxc

- deploy:
    role:
    - host
    namespace: device
    timeout:
      minutes: 4
    to: tftp
    # authorize for ssh adds the ssh public key to authorized_keys
    authorize: ssh

    kernel:
      url: http://files.lavasoftware.org/components/lava/standard/debian/jessie/armhf/4/vmlinuz
      sha256sum: 37340d10427619ca4821f51c5d780ae930d799c905f903ef5036b00187b65f0f
      type: zimage
    ramdisk:
      url: http://files.lavasoftware.org/components/lava/standard/debian/jessie/armhf/4/initramfs.cpio.gz
      sha256sum: d5e80d4fc9b6e69f112930ddb744caaba2869ffa22782590f7669321c08feca5
      compression: gz
      # the bootloader needs a u-boot header on the modified ramdisk
      add-header: u-boot
    modules:
      url: http://files.lavasoftware.org/components/lava/standard/debian/jessie/armhf/4/modules.tar.gz
      sha256sum: 907d0fb96ffb18f7b1003ce899aeababbb7caeee3b4cebace57fd144a7bf8b1a
      compression: gz
    nfsrootfs:
      url: http://files.lavasoftware.org/components/lava/standard/debian/jessie/armhf/4/jessie-armhf-nfs.tar.gz
      sha256sum: bb249f1d2d1e8d78885479cf32f9e63e4f4036980e9ae5cb38a42a8bb9b6d3aa
      compression: gz
    dtb:
      url: http://files.lavasoftware.org/components/lava/standard/debian/jessie/armhf/4/dtbs/am335x-boneblack.dtb
      sha256sum: 19559c27014bb8cccea9c1c2a62d1e7a3ded18ebfd0b6a34d276d71918dfc917

- deploy:
    role:
    - guest
    namespace: guest
    timeout:  # timeout for the ssh connection attempt
      seconds: 30
    to: ssh
    connection: ssh
    protocols:
      lava-multinode:
      - action: prepare-scp-overlay
        request: lava-wait
        # messageID matches hostID
        messageID: ipv4
        message:
          # the key of the message matches value of the host_key
          # the value of the message gets substituted
          ipaddr: $ipaddr
      timeout:  # delay_start timeout
        minutes: 5

- boot:
    role:
    - host
    namespace: device
    timeout:
      minutes: 15
    method: u-boot
    commands: nfs
    auto_login:
      login_prompt: 'login:'
      username: root
    prompts:
    - 'root@jessie:'
    parameters:
      shutdown-message: "reboot: Restarting system"

- boot:
    role:
    - guest
    namespace: guest
    connection: ssh
    timeout:
      minutes: 3
    prompts:
    - 'root@jessie:'
    parameters:
      hostID: ipv4  # messageID
      host_key: ipaddr  # message key
    method: ssh

- test:
    role:
    - host
    namespace: device
    timeout:
      minutes: 30
    definitions:
    - repository:
        metadata:
          format: Lava-Test Test Definition 1.0
          name: install-ssh
          description: "install step"
          os:
          - debian
          scope:
          - functional
        run:
          steps:
          - apt-get update -q
          - DEBIAN_FRONTEND=noninteractive lava-test-case install-base --shell apt-get -q -y install -o Dpkg::Options::="--force-confold" openssh-server ntpdate net-tools
          - ntpdate-debian
          - lava-network broadcast eth0
          # messageID matches, message_key as the key.
          - lava-send ipv4 ipaddr=$(lava-echo-ipv4 eth0)
          - lava-send lava_start
          - lava-sync clients
      from: inline
      name: ssh-inline
      path: inline/ssh-install.yaml
    - repository: http://git.linaro.org/lava-team/lava-functional-tests.git
      from: git
      path: lava-test-shell/smoke-tests-basic.yaml
      name: smoke-tests
    - repository: http://git.linaro.org/lava-team/lava-functional-tests.git
      from: git
      path: lava-test-shell/single-node/singlenode02.yaml
      name: singlenode-intermediate

- test:
    role:
    - guest
    namespace: guest
    timeout:
      minutes: 5
    definitions:
    - repository: http://git.linaro.org/lava-team/lava-functional-tests.git
      from: git
      path: lava-test-shell/smoke-tests-basic.yaml
      name: smoke-tests
      # run the inline last as the host is waiting for this final sync.
    - repository:
        metadata:
          format: Lava-Test Test Definition 1.0
          name: client-ssh
          description: "client complete"
          os:
          - debian
          scope:
          - functional
        run:
          steps:
          - df -h
          - free
          - lava-sync clients
      from: inline
      name: ssh-client
      path: inline/ssh-client.yaml

- test:
    role:
    - host
    namespace: probe
    timeout:
      minutes: 5
    definitions:
    - repository:
        metadata:
          format: Lava-Test Test Definition 1.0
          name: network
          description: "installation"
          os:
          - debian
          scope:
          - functional
        run:
          steps:
          - apt-get update -q
          - DEBIAN_FRONTEND=noninteractive lava-test-case install-base --shell apt-get -q -y install -o Dpkg::Options::="--force-confold" net-tools
          - lava-test-case inline --shell ls /dev/serial/by-id
      from: inline
      name: lxc-test
      path: inline/lxc-test.yaml

- test:
    role:
    - host
    namespace: device
    timeout:
      minutes: 10
    definitions:
    - repository: http://git.linaro.org/lava-team/lava-functional-tests.git
      from: git
      path: lava-test-shell/single-node/singlenode03.yaml
      name: singlenode-advanced
