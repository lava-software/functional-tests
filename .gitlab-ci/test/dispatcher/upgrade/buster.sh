#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  set -x
  apt-get update -q
  # Install dependencies
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes ca-certificates gnupg wget
  # Install lava-dispatcher from release
  echo "deb http://apt.lavasoftware.org/release buster main" > /etc/apt/sources.list.d/lava-release.list
  wget https://apt.lavasoftware.org/lavasoftware.key.asc
  apt-key add lavasoftware.key.asc
  apt-get update -q
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes lava-dispatcher lava-lxc-mocker
  dpkg-query -W lava-dispatcher
  # Prepare to install from daily
  echo "deb http://apt.lavasoftware.org/daily buster main" > /etc/apt/sources.list.d/lava-daily.list
else
  set -x
  apt-get update -q
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes lava-dispatcher
fi
