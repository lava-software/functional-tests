#!/bin/sh

# Publish the pipeline result to lavafed at https://federation.lavasoftware.org
# The script grab the candidate package from daily to guess the package version
# that was tested in the previous pipeline stages.

set -e

PUBLISH_URL="https://federation.lavasoftware.org/api/v0.1/jobs/"

if [ "$1" = "setup" ]
then
  set -x
  apt-get update -q
  apt-get -q install --no-install-recommends --yes ca-certificates curl gnupg wget
  wget https://apt.lavasoftware.org/lavasoftware.key.asc
  apt-key add lavasoftware.key.asc
  echo "deb http://apt.lavasoftware.org/daily buster main" > /etc/apt/sources.list.d/lava-daily.list
else
  # Check if PUBLISH_TOKEN is defined
  if [ -z "$PUBLISH_TOKEN" ]
  then
    echo "PUBLISH_TOKEN is empty"
    exit 1
  fi

  apt-get update -q
  # Grab the candidate version
  version=$(apt-cache policy lava-dispatcher | grep "Candidate" | awk '{print $2}' | cut -d"+" -f1)

  # Publish to lavafed
  curl -X POST -H "Authorization: $PUBLISH_TOKEN" $PUBLISH_URL --data "{\"id\": $CI_PIPELINE_ID, \"definition\": \"metadata:\n  'device.type': 'package'\n  'lab.name': 'package'\n  'device.name': 'package'\n  'slave.version': '$version'\n  'job.name': 'package install/upgrade'\n  'slave.arch': 'amd64'\n  'job.type': 'package'\n  'job.url': 'https://git.lavasoftware.org/lava/functional-tests/pipelines/{id}'\n  features.0.name: 'install/upgrade'\n  features.0.type: 'generic'\n  features.0.description: 'Install and upgrade packages'\n  features.0.action: 'none'\", \"health_string\": \"Incomplete\"}"
fi
